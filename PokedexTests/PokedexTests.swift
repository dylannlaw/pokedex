//
//  PokedexTests.swift
//  PokedexTests
//
//  Created by Dylan Law Wai Chun on 02/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import XCTest
@testable import Pokedex

class PokedexTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testConnectAPI() {
        let expectation = self.expectationWithDescription("Load Pokemon")
        
        let url2: NSURLComponents = NSURLComponents.init(string: "https://pokeapi.co/api/v2/pokemon/")!
        url2.query = "limit=1000"
        
        print(url2.URL!)
        
        let session = NSURLSession.sharedSession()
        
        var pokemons: [Pokemon] = []
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url2.URL!) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            
            let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions (rawValue: 0)) as! [String : AnyObject]
            
            if let resultArray: [ [String : AnyObject] ] = json["results"] as? [ [String : AnyObject] ] {
                for jsonData in resultArray {
                    let name: String = jsonData["name"] as! String
                    print(name)
                    let pokemon: Pokemon = Pokemon(name: name)
                    pokemons.append(pokemon)
                    
                    XCTAssert(!pokemons.isEmpty, "Should have some pokemons.")
                }
            }
            
            expectation.fulfill()
        }
        
        loadTask.resume()
        self.waitForExpectationsWithTimeout(20, handler: nil)
        
    }

    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
