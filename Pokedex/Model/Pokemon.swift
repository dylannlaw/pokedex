//
//  Pokemon.swift
//  Pokedex
//
//  Created by Dylan Law Wai Chun on 03/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import Foundation

class Pokemon {
    var name: String
    
    init(name: String) {
        self.name = name
    }
    
}