//
//  NetworkLoader.swift
//  Pokedex
//
//  Created by Dylan Law Wai Chun on 03/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//

import Foundation

class NetworkLoader {
    static let sharedLoader: NetworkLoader = NetworkLoader()
    
    private var url: NSURLComponents = NSURLComponents.init(string: "https://pokeapi.co/api/v2/pokemon/")!
    
    
    let session = NSURLSession.sharedSession()
    
    
    func loadPokemon(completion: ([Pokemon]) -> Void)  {
        url.query = "limit=1000"
        var pokemons: [Pokemon] = []
        let loadTask: NSURLSessionDataTask = session.dataTaskWithURL(url.URL!) {
            (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            
            if (data != nil) {
                if let json: [String : AnyObject] = try! NSJSONSerialization.JSONObjectWithData(data!, options:    NSJSONReadingOptions (rawValue: 0)) as? [String : AnyObject] {
        
                    if let resultArray: [ [String : AnyObject] ] = json["results"] as? [ [String : AnyObject] ] {
                        for jsonData in resultArray {
                            let name: String = jsonData["name"] as! String
                            NSLog(name)
                            let pokemon: Pokemon = Pokemon(name: name)
                    
                            pokemons.append(pokemon)
                   
                        }
                    }
                }
            }
            completion(pokemons)
        }        
        loadTask.resume()
        
    }
}