//
//  PokedexViewController.swift
//  Pokedex
//
//  Created by Dylan Law Wai Chun on 03/08/2016.
//  Copyright © 2016 Dylan. All rights reserved.
//


import UIKit

class PokedexViewController: UITableViewController {
    var pokemons: [Pokemon] = []
    var pokedexRefreshControl: UIRefreshControl!
    @IBOutlet var pokeDexTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.pokedexRefreshControl = UIRefreshControl()
        self.tableView.addSubview(pokedexRefreshControl)
        self.pokedexRefreshControl.addTarget(self, action: #selector(PokedexViewController.refreshPokedex), forControlEvents: UIControlEvents.ValueChanged)
        refreshPokedex()
    }
    
    func refreshPokedex() {
        NetworkLoader.sharedLoader.loadPokemon({ pokemons in
            self.pokemons = pokemons
            dispatch_async(dispatch_get_main_queue(), {
                self.pokedexRefreshControl.endRefreshing()
                // Only this in main thread
                 self.tableView.reloadData()
            })
        });
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier = "PokedexTableViewCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! PokedexTableViewCell
        
        let pokemon = pokemons[indexPath.row]
        
        cell.pokemon = pokemon
        
        return cell
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return pokemons.count
    }
    
}

class PokedexTableViewCell: UITableViewCell {
    // MARK: Properties
    
    @IBOutlet weak var nameLabel: UILabel!
    
    var pokemon: Pokemon! {
        didSet {
            updateDisplay()
        }
    }
    
    func updateDisplay() {
        self.nameLabel.text = pokemon.name
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
